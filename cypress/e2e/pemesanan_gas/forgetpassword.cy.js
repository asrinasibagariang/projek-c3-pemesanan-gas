describe("visit ForgetPassword", () => {
  beforeEach(() => {
    // Cypress starts out with a blank slate for each test
    // so we must tell it to visit our website with the `cy.visit()` command.
    // Since we want to visit the same URL at the start of all our tests,
    // we include it in our beforeEach function so that it runs before each test
    cy.visit("https://fepemesanangas.herokuapp.com/ForgetPassword");
  });
  it("show the navbar", () => {
    cy.get(".navbar").should("be.visible");

    cy.get(".navbar").should("have.css", "background-color").and("eq", "rgb(70, 57, 209)");
  });
  it("show text", () => {
    cy.get('input[name="Email"]').should("be.visible");
  });
  it("show Kirim kode OTP button", () => {
    cy.get(".button").should("be.visible");
  });
});
