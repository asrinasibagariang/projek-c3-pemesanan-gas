describe("visit dashboardadmin", () => {
  beforeEach(() => {
    // Cypress starts out with a blank slate for each test
    // so we must tell it to visit our website with the `cy.visit()` command.
    // Since we want to visit the same URL at the start of all our tests,
    // we include it in our beforeEach function so that it runs before each test
    cy.visit("https://fepemesanangas.herokuapp.com/dashboardadmin");
  });
  it("show the navbar", () => {
    cy.get(".navbar").should("be.visible");

    cy.get(".navbar").should("have.css", "background-color").and("eq", "rgb(70, 57, 209)");
  });
  it("show Frame", () => {
    cy.get(".frame").should("be.visible");
  });
  it("show login wrapper", () => {
    cy.get(".login-wrapper").should("be.visible");
  });
  it("show title welcome to Mola dashboard", () => {
    cy.get(".title").should("be.visible");
  });
  it("login-wrapper", () => {
    cy.get(".login-wrapper.info").should("be.visible");
  });
  it("Info-item", () => {
    cy.get(".info-item").should("be.visible");
  });
  it("Info-item", () => {
    cy.get(".info-item").should("be.visible");
  });
  it("Info-item", () => {
    cy.get(".info-item").should("be.visible");
  });
  it("Info-item", () => {
    cy.get(".info-item").should("be.visible");
  });
  it("Info-item", () => {
    cy.get(".info-item").should("be.visible");
  });
  it("Info-item", () => {
    cy.get(".info-item").should("be.visible");
  });
  it("Info-item", () => {
    cy.get(".info-item").should("be.visible");
  });
  it("Info-item", () => {
    cy.get(".info-item").should("be.visible");
  });
  it("Info-item", () => {
    cy.get(".info-item").should("be.visible");
  });
  it("Info-item", () => {
    cy.get(".info-item").should("be.visible");
  });
});
